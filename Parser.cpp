//
// Created by Savely on 13.10.17.
//
#include <iostream>
#include <fstream>
#include <cmath>
#include "Parser.h"


void Rules::clearInteractiveArguments(char *firstArgument, char *secondArgument) {
    for (int k = 0; k < 10; ++k) {
        firstArgument[k] = '\0';
        secondArgument[k] = '\0';
    }
}


void Rules::nextStep(){
    for(int i=y-1; i>=0;i--)
    {
        for(int j=0;j<x;j++) {
            if(previousGameField[i][j]=='.')
            {
                if(liveAroundCounter(y-i,j+1)==M)
                {
                    nextGameField[i][j]='*';
                }
            }
            else
            {
                if((liveAroundCounter(y-i,j+1)<N)||(liveAroundCounter(y-i,j+1)>K))
                {
                    nextGameField[i][j]='.';
                }
                else
                {
                    nextGameField[i][j]='*';
                }
            }
        }
    }
}


void Rules::fieldCopy(char ** previous, char ** next) {
    for(int i=0; i<y;i++)
    {
        for(int j=0;j<x;j++)
        {
            next[i][j] = previous[i][j];
        }
    }
}


void Rules::clearField(char ** field) {
    for(int i=0;i<y;i++)
    {
        for(int j=0;j<x;j++)
        {
            field[i][j]='.';
        }
    }
}


void Rules::getArgument(const char * buf, char* res, int nubmer) {
    int cursorI=0;
    for (int j = 0; j < 10; ++j) {
        if((buf[j]==32)&&(nubmer==1))
        {
            j++;
            while(buf[j]!=32)
            {
                if(buf[j]=='\0')
                {
                    break;
                }
                res[cursorI]=buf[j];
                j++;
                cursorI++;
            }
        }
        else if((nubmer > 1)&&(buf[j]==32))
        {
            nubmer--;
        }
    }
}


int Rules::strToInt(char * buf) {
    int res = 0;
    double currentPow = pow(10,strlen(buf)-1);
    for(int i=0; i<std::strlen(buf);i++)
    {
        res += (buf[i]-'0')*currentPow;
        currentPow /= 10;
    }
    return res;
}


int Rules::buildGameField() {

    bool rulesFlag = false;
    bool fieldFlag = false;
    std::ifstream fin(input);
    if (!fin.is_open())
    {
        std::cout<<"Error opening the input file"<<std::endl;
        return -1;
    }
    if((!fieldSetByKeys)||(!mnkSetByKeys))
    {
        char buff[10] ={0};
        while (!fin.eof())
        {
            fin >> buff;
            if (buff[0]!='#')
            {
                if (std::strcmp(buff,"FIELD")==0)
                {
                    if (!fieldFlag)
                    {
                        if (!fieldSetByKeys)
                        {
                            fin >> buff;
                            x = strToInt(buff);
                            fin >> buff;
                            y = strToInt(buff);
                        }
                        fieldFlag = true;
                    }
                    else
                    {
                        std::cout<<"The presence of 2 or more teams FIELD"<<std::endl;
                        return -2;
                    }
                }
                if (std::strcmp(buff,"RULES")==0)
                {
                    if(!rulesFlag)
                    {
                        if(!mnkSetByKeys)
                        {
                            fin >> buff;
                            M = strToInt(buff);
                            fin >> buff;
                            N = strToInt(buff);
                            fin >> buff;
                            K  = strToInt(buff);
                        }
                        rulesFlag = true;
                    }
                    else
                    {
                        std::cout<<"the presence of 2 or more teams RULES"<<std::endl;
                        return -3;
                    }
                }
            }
        }
    }
    char buff[10] ={0};
    fin.clear();
    fin.seekg(0, std::ios_base::beg);
    nextGameField = new char *[y];
    for(int i=0; i<y;i++)
    {
        nextGameField[i] = new char[x];
    }
    previousGameField = new char * [y];
    for(int i=0; i<y;i++)
    {
        previousGameField[i] = new char[x];
    }
    clearField(nextGameField);
    clearField(previousGameField);
    while (!fin.eof())
    {
        fin>>buff;
        if(std::strcmp(buff,"SET") == 0)
        {
            char a[15]={0};
            char b[15]={0};
            fin >> a;
            fin >> b;
            previousGameField[strToInt(b)][strToInt(a)]='*';
        }
        else if ((std::strcmp(buff,"FIELD")==0)||(std::strcmp(buff,"RULES")==0)||(buff[0]=='#')) {}
        else
        {
            std::cout<<"Found unknown command \n";
            return -4;
        }
    }
    return 0;
}


int Rules::liveAroundCounter(int firstCoordinate, int secondCoordinate) {
    int res = 0;
    int xCoordinate = y-firstCoordinate-1;
    int yCoordinate = secondCoordinate-2;

    if (xCoordinate == -1) {xCoordinate = y-1;}
    else if (xCoordinate == y) {xCoordinate = 0;}

    if (yCoordinate == -1) {yCoordinate=x-1;}
    else if (yCoordinate == x) {yCoordinate = 0;}

    if(previousGameField[xCoordinate++][yCoordinate]=='*')
    {
        res++;
    }
    if (xCoordinate == -1) {xCoordinate = y-1;}
    else if (xCoordinate == y) {xCoordinate = 0;}

    if (yCoordinate == -1) {yCoordinate=x-1;}
    else if (yCoordinate == x) {yCoordinate = 0;}

    if(previousGameField[xCoordinate++][yCoordinate]=='*')
    {
        res++;
    }
    if (xCoordinate == -1) {xCoordinate = y-1;}
    else if (xCoordinate == y) {xCoordinate = 0;}

    if (yCoordinate == -1) {yCoordinate=x-1;}
    else if (yCoordinate == x) {yCoordinate = 0;}

    if(previousGameField[xCoordinate][yCoordinate++]=='*')
    {
     res++;
    }
    if (xCoordinate == -1) {xCoordinate = y-1;}
    else if (xCoordinate == y) {xCoordinate = 0;}

    if (yCoordinate == -1) {yCoordinate=x-1;}
    else if (yCoordinate == x) {yCoordinate = 0;}

    if(previousGameField[xCoordinate][yCoordinate++]=='*')
    {
       res++;
    }
    if (xCoordinate == -1) {xCoordinate = y-1;}
    else if (xCoordinate == y) {xCoordinate = 0;}

    if (yCoordinate == -1) {yCoordinate=x-1;}
    else if (yCoordinate == x) {yCoordinate = 0;}

    if(previousGameField[xCoordinate--][yCoordinate]=='*')
    {
        res++;
    }
    if (xCoordinate == -1) {xCoordinate = y-1;}
    else if (xCoordinate == y) {xCoordinate = 0;}

    if (yCoordinate == -1) {yCoordinate=x-1;}
    else if (yCoordinate == x) {yCoordinate = 0;}

    if(previousGameField[xCoordinate--][yCoordinate]=='*')
    {
        res++;
    }
    if (xCoordinate == -1) {xCoordinate = y-1;}
    else if (xCoordinate == y) {xCoordinate = 0;}

    if (yCoordinate == -1) {yCoordinate=x-1;}
    else if (yCoordinate == x) {yCoordinate = 0;}

    if(previousGameField[xCoordinate][yCoordinate--]=='*')
    {
        res++;
    }
    if (xCoordinate == -1) {xCoordinate = y-1;}
    else if (xCoordinate == y) {xCoordinate = 0;}

    if (yCoordinate == -1) {yCoordinate=x-1;}
    else if (yCoordinate == x) {yCoordinate = 0;}

    if(previousGameField[xCoordinate][yCoordinate]=='*')
    {
        res++;
    }

    return res;
}


int Rules::checkArguments(char * argumetnts) {
    for (int i = 0; i < std::strlen(argumetnts); ++i) {
        if((argumetnts[i]<48)||(argumetnts[i]>57))
        {
            return -1;
        }
        if(argumetnts[i]=='-')
        {
            return -2;
        }
    }
    return 0;
}


int Rules::countArgumets(char * data) {
    int res = 0;
    for (int i = 0; i < std::strlen(data); ++i) {
        if(data[i]==' ')
        {
            while(data[i]==' ')
            {
                i++;
                if(data[i]!=' ')
                {
                    if(data[i]!=0)
                    {
                        res++;
                        break;
                    }
                }
                if(i==std::strlen(data))
                {
                    break;
                }
            }
        }
    }
    return res;
}


bool Rules::checkerLiveInTheWorld(char** field) {
    for(int k=0;k<y;k++)
    {
        for(int t=0;t<x;t++)
        {
            if(field[k][t] == '*')
            {
                return true;
            }
        }
    }
    return false;
}


bool Rules::checkerStableConfiguration() {
    for(int i=0;i<y;i++)
    {
        for(int j=0; j<x;j++)
        {
            if (previousGameField[i][j]!=nextGameField[i][j])
            {
                return false;
            }
        }
    }
    return true;
}


void Rules::writeResultOfGame(){
    std::ofstream fout(output);
    if ((M!=3)||(N!=2)||(K!=3))
    {
        fout<<"M = "<<M<<std::endl;
        std::cout<<"M = "<<M<<std::endl;
        fout<<"N = "<<N<<std::endl;
        std::cout<<"N = "<<N<<std::endl;
        fout<<"K = "<<K<<std::endl;
        std::cout<<"K = "<<K<<std::endl;
    }
    for(int i=0; i<y;i++)
    {
        for(int j=0;j<x;j++)
        {
            fout<<previousGameField[i][j];
            std::cout<<previousGameField[i][j];
        }
        fout<<"\n";
        std::cout<<std::endl;
    }
}


Rules::Rules(int keysCount, char *keysData[]) {

    bool in = false;

    for(int i = 1; i<keysCount;i++)
    {
        if((std::strcmp(keysData[i],"-m")) == 0)
        {
            mnkSetByKeys = true;
            M = strToInt(keysData[i+1]);
            i++;
        }
        else if((std::strcmp(keysData[i],"-n")) == 0)
        {
            mnkSetByKeys = true;
            N = strToInt(keysData[i+1]);
            i++;
        }
        else if((std::strcmp(keysData[i],"-k")) == 0)
        {
            mnkSetByKeys = true;
            K = strToInt(keysData[i+1]);
            i++;
        }
        else if(((std::strcmp(keysData[i],"-f")) == 0)||((std::strcmp(keysData[i],"-field")) == 0))
        {
            fieldSetByKeys = true;
            x = strToInt(keysData[i+1]);
            y = strToInt(keysData[i+2]);
            i+=2;
        }
        else if(((std::strcmp(keysData[i],"-ic")) == 0)||((std::strcmp(keysData[i],"--iterations")) == 0))
        {
            iterationCounter = strToInt(keysData[i+1]);
            i++;
        }
        else if(((std::strcmp(keysData[i],"-if")) == 0)||((std::strcmp(keysData[i],"--input")) == 0))
        {
            in = true;
            input = keysData[i+1];
            i++;
        }
        else if(((std::strcmp(keysData[i],"-o")) == 0)||((std::strcmp(keysData[i],"--output")) == 0))
        {
            output = keysData[i+1];
            i++;
        }
        else if(((std::strcmp(keysData[i],"-h")) == 0)||((std::strcmp(keysData[i],"--help")) == 0))
        {
            std::cout<<" Long flags:\n"
                    "--input <input file>\n"
                    "--output <output file>\n"
                    "--iterations <iteration count>\n"
                    "--field <x> <y>\n"
                    "--help\n"
                    "\n"
                    "Short flags:\n"
                    "-if <input file>\n"
                    "-o <output file>\n"
                    "-ic <iteration count>\n"
                    "-m <m>\n"
                    "-n <n>\n"
                    "-k <k>\n"
                    "-f <x> <y>\n"
                    "-h  "<<std::endl;
            return ;
        }
        else
        {
            std::cout<<"Unknown key, <"<<keysData[i]<<"> try -h or --help\n";
            return;
        }
    }
    if(!in)
    {
        std::cout<<"input file is missing\n";
        return;
    }
    int checker = buildGameField();
    if (checker == 0)
    {
        for(int i=0;i<iterationCounter;i++)
        {

            nextStep();
            if(checkerStableConfiguration())
            {
                std::cout<<"Stable configuration fields";
                break ;
            }
            fieldCopy(nextGameField,previousGameField);
            clearField(nextGameField);
            if(!checkerLiveInTheWorld(previousGameField))
            {
                std::cout<<"All organisms have died";
                break;
            }
        }
        writeResultOfGame();
    }
}


Rules::Rules() {
    int stepCounter=1;
    bool help = false;
    nextGameField = new char * [y];
    for(int i=0; i<y;i++)
    {
        nextGameField[i] = new char[x];
    }
    previousGameField = new char * [y];
    for(int i=0; i<y;i++)
    {
        previousGameField[i] = new char[x];
    }
    clearField(nextGameField);
    clearField(previousGameField);
    char  command[20]={0};
    char firstArgument[10]={0};
    char secondArgument[10]={0};
    int checker = 0;
    int argCount;
    while(true)
    {
        std::cin.getline(command,20);
        argCount = countArgumets(command);
        if(std::strcmp(std::strncpy(firstArgument,command,4),"exit")==0)
        {
            return ;
        }
        if((std::strcmp(std::strncpy(firstArgument,command,4),"step")==0)
           &&((command[4]==' ')||(command[4]==0)))
        {
            if(argCount > 1)
            {
                std::cout<<"Too many argumets\n\n";
            }
            else
            {
                clearInteractiveArguments(firstArgument,secondArgument);
                getArgument(command,firstArgument,1);
                checker = checkArguments(firstArgument);
                if(checker<0)
                {
                    std::cout<<"Invalid data\n"
                            "step N – Evolve to N generations "
                            "(N may be absent, then is assumed to be 1)\n\n" <<std::endl;
                }
                else
                {
                    help = true;
                    if(std::strlen(command)==4)
                    {
                        clearInteractiveArguments(firstArgument,secondArgument);
                        firstArgument[0]='1';
                    }
                    for (int l = 0; l < strToInt(firstArgument); ++l) {
                        fieldCopy(nextGameField,previousGameField);
                        nextStep();
                        stepCounter++;
                        if(checkerStableConfiguration())
                        {
                            std::cout<<"Stable configuration fields\n";
                            for(int i=0;i<y;i++)
                            {
                                for(int j=0; j<x;j++)
                                {
                                    std::cout<<nextGameField[i][j];
                                }
                                std::cout<<"\n";
                            }
                            return;
                        }
                        if(!checkerLiveInTheWorld(nextGameField))
                        {
                            std::cout<<"All organisms have died\n";
                            for(int i=0;i<y;i++)
                            {
                                for(int j=0; j<x;j++)
                                {
                                    std::cout<<nextGameField[i][j];
                                }
                                std::cout<<"\n";
                            }
                            return;
                        }
                    }
                }
            }
        }
        clearInteractiveArguments(firstArgument,secondArgument);
        if((std::strcmp(std::strncpy(firstArgument,command,5),"reset")==0)
           &&((command[5]==0)||(command[5]==' ')))
        {
            if(argCount != 0)
            {
                std::cout<<"Too many argumets\n";
            }
            else
            {
                fieldCopy(nextGameField,previousGameField);
                clearField(nextGameField);
                stepCounter = 1;
            }
            help = true;
        }
        clearInteractiveArguments(firstArgument,secondArgument);
        if((std::strcmp(std::strncpy(firstArgument,command,3),"set")==0)
           &&(command[3]==' '))
        {
            if(argCount != 2)
            {
                std::cout<<"Too many argumets\n\n";
            }
            else
            {
                clearInteractiveArguments(firstArgument,secondArgument);
                fieldCopy(nextGameField,previousGameField);
                getArgument(command,firstArgument,1);
                checker = checkArguments(firstArgument);
                if(checker<0)
                {
                    std::cout<<"Invalid data\n"
                            "set X Y — Set the body in a cell\n\n"<<std::endl;
                }
                else
                {
                    getArgument(command,secondArgument,2);
                    checker = checkArguments(secondArgument);
                    if(checker<0)
                    {
                        std::cout<<"Invalid data\n"
                                "set X Y — Set the body in a cell\n"<<std::endl;
                    }
                    else
                    {
                        if((strToInt(secondArgument)>=y)||(strToInt(firstArgument)>=x))
                        {
                            std::cout<<"Invalid data\n"
                                    "set X Y — Set the body in a cell\n"<<std::endl;
                        }
                        else
                        {
                            nextGameField[strToInt(secondArgument)][strToInt(firstArgument)] = '*';
                            help = true;
                        }
                    }
                }
            }
        }
        clearInteractiveArguments(firstArgument,secondArgument);
        if((std::strcmp(std::strncpy(firstArgument,command,5),"clear")==0)
           &&(command[5]==' '))
        {
            if(argCount != 2)
            {
                std::cout<<"Too many argumets\n\n";
            }
            else
            {
                fieldCopy(nextGameField,previousGameField);
                clearInteractiveArguments(firstArgument,secondArgument);
                getArgument(command,firstArgument,1);
                checker = checkArguments(firstArgument);
                if(checker<0)
                {
                    std::cout<<"Invalid data\n"
                            "clear X Y – Clear cell\n\n"<<std::endl;
                }
                else
                {
                    getArgument(command,secondArgument,2);
                    checker = checkArguments(secondArgument);
                    if(checker<0)
                    {
                        std::cout<<"Invalid data\n"
                                "clear X Y – Clear cell\n"<<std::endl;
                    }
                    else
                    {
                        if((strToInt(secondArgument)>y)||(strToInt(firstArgument)>x))
                        {
                            std::cout<<"Invalid data\n"
                                    "clear X Y – Clear cell\n"<<std::endl;
                        }
                        else
                        {
                            nextGameField[strToInt(secondArgument)][strToInt(firstArgument)] = '.';
                            help = true;
                        };

                    }
                }
            }
        }
        clearInteractiveArguments(firstArgument,secondArgument);
        if((std::strcmp(std::strncpy(firstArgument,command,4),"back")==0)
            &&((command[4]==0)||(command[4]==' ')))
        {
            if(argCount != 0)
            {
                std::cout<<"Too many argumets\n\n";
            }
            fieldCopy(previousGameField,nextGameField);
            help = true;
        }
        clearInteractiveArguments(firstArgument,secondArgument);
        if((std::strcmp(std::strncpy(firstArgument,command,4),"save")==0)
            &&(command[4]==' '))
        {
            if(argCount != 1)
            {
                std::cout<<"Too many argumets\n\n";
            }
            else
            {
                clearInteractiveArguments(firstArgument,secondArgument);
                getArgument(command,firstArgument,1);
                std::ofstream fout(firstArgument);
                for(int i=0; i<y;i++)
                {
                    for(int j=0;j<x;j++)
                    {
                        fout<<nextGameField[i][j];
                    }
                    if(i!=y-1)
                    {
                        fout<<"\n";
                    }
                }
                help = true;
            }
        }
        clearInteractiveArguments(firstArgument,secondArgument);
        if((std::strcmp(std::strncpy(firstArgument,command,4),"load")==0)
           &&(command[4]==' '))
        {
            if(argCount != 1)
            {
                std::cout<<"Too many argumets\n\n";
            }
            else
            {
                char xBuf[x];
                int yBuf =0;
                clearInteractiveArguments(firstArgument,secondArgument);
                getArgument(command,firstArgument,1);
                std::ifstream fin(firstArgument);
                if (!fin.is_open())
                {
                    std::cout<<"Error opening the input file\n"<<std::endl;
                    return;
                }

                while (!fin.eof())
                {
                    fin>>xBuf;
                    for (int q = 0; q < x; ++q) {
                        nextGameField[yBuf][q]=xBuf[q];
                    }
                    yBuf++;
                }
                stepCounter = 1;
                help = true;
            }
        }
        clearInteractiveArguments(firstArgument,secondArgument);
        if((std::strcmp(std::strncpy(firstArgument,command,5),"rules")==0)
         &&(command[5]==' '))
        {
            if(argCount != 3)
            {
                std::cout<<"Too many argumets\n\n";
            }
            else
            {
                clearInteractiveArguments(firstArgument,secondArgument);
                getArgument(command,firstArgument,1);
                checker = checkArguments(firstArgument);
                if(checker<0)
                {
                    std::cout<<"Invalid data\n"
                            "rules m n k - Set M, N, K\n\n"<<std::endl;
                }
                else
                {
                    M=strToInt(firstArgument);
                    clearInteractiveArguments(firstArgument,secondArgument);
                    getArgument(command,firstArgument,1);
                    if(checker<0)
                    {
                        std::cout<<"Invalid data\n"
                                "rules m n k - Set M, N, K\n\n"<<std::endl;
                    }
                    else
                    {
                        N=strToInt(firstArgument);
                        clearInteractiveArguments(firstArgument,secondArgument);
                        getArgument(command,firstArgument,1);
                        if(checker<0)
                        {
                            std::cout<<"Invalid data\n"
                                    "rules m n k - Set M, N, K\n\n"<<std::endl;
                        }
                        else
                        {
                            K=strToInt(firstArgument);
                            help = true;
                        }
                    }
                }
            }
        }
        clearInteractiveArguments(firstArgument,secondArgument);
        if (help)
        {
            std::cout<<"Step №"<<stepCounter<<std::endl;
            for(int i=0;i<y;i++)
            {
                for(int j=0; j<x;j++)
                {
                    std::cout<<nextGameField[i][j];
                }
                std::cout<<"\n";
            }
        }
        else
        {
            std::cout<<"Invalid command\n"
                    "here's a list of available commands\n"
                    "set X Y — Set the body in a cell \n"
                    "clear X Y – Clear cell\n"
                    "step N – Evolve to N generations "
                    "(N may be absent, then is assumed to be 1)\n"
                    "back – Return the previous configuration \n"
                    "save “filename” – Save the state of the game field"
                    "in a text file in the current directory (without the history moves)\n"
                    "load “filename” – Load the state of the game field "
                    "from a text file in the current directory "
                    "(with zeroing the counter moves)\n"
                    "rules m n k - Set M, N, K"<<std::endl;
        }
        help = false;
        for (int m = 0; m < 19; ++m) {
            command[m] = 0;
        }
    }
}


Rules::~Rules() {
    if(nextGameField != nullptr)
    {
        for(int i=0; i<y;i++)
        {
            delete [] nextGameField[i];
            nextGameField[i] = nullptr;
        }
        delete [] nextGameField;
        *nextGameField = nullptr;
    }

    if(previousGameField != nullptr)
    {
        for(int i=0; i<y;i++)
        {
            delete [] previousGameField[i];
            previousGameField[i] = nullptr;
        }
        delete [] previousGameField;
        *previousGameField = nullptr;
    }
}