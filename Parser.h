//
// Created by Savely on 13.10.17.
//

#ifndef GAMEOFLIFE_PARSER_H
#define GAMEOFLIFE_PARSER_H

#include <string>

class Rules {
private:
    void clearInteractiveArguments(char*,char*);
    void nextStep();
    void fieldCopy(char**,char**);
    void clearField(char**);
    void getArgument(const char*,char*,int);
    int strToInt(char*);
    int buildGameField();
    int liveAroundCounter(int, int);
    int checkArguments(char*);
    int countArgumets(char*);
    bool checkerLiveInTheWorld(char**);
    bool checkerStableConfiguration();
    void writeResultOfGame();

public:

    int M = 3;
    int N = 2;
    int K = 3;
    int x = 10;
    int y = 10;
    bool fieldSetByKeys = false;
    bool mnkSetByKeys = false;
    char **previousGameField;
    char **nextGameField;
    char* input;
    char* output;
    int iterationCounter= 10;

    Rules();
    Rules(int,char*[]);

    ~Rules();
};


#endif //GAMEOFLIFE_PARSER_H
